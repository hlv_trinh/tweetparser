var parser = require('./tweet_parser');

var test1 = '@chris you around?';
var test2 = 'Good morning! (megusta) (coffee)';
var test3 = 'Olympics are starting soon; http://www.nbcolympics.com';
var test4 = "@bob @john_ @_11___john_123_ (success) @congcông @ひらがな such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
var test5 = 'Why and how we ditched the good old select element" by @mibosc http://buff.ly/1Mg4sn4';
var test6 = 'IAB Releases A New Overview Of In-Feed Ads http://tcrn.ch/1OuPV69  by @anthonyha';
var test7 = 'LIVE on #Periscope: Walking through #AppNation #IoTSummit! Whose ready for #LevisIoTHack?!  https://t.co/XFfby7VSR2';
var test8 = 'Religious institutions are driven by male leadership, and they create policies in their likeness." http://t.ted.com/UeaID91';
var test9 = 'A startup founder jumped to her death from a rooftop bar in Manhattan http://read.bi/1Ki1O0E  :(';
var test10 = 'Working the @AngelHack #LevisIoTHack @LevisStadium today & SUHHH excited! #IoTSummit #AppNation ';
var test11 = 'We were named a Leader by @Gartner_Inc in their Application Services Governance & #API mgmt MQ. Read the report: http://ow.ly/LIt8v  #CIO';

var testCase = [test1, test2, test3, test4, test5, test6, test7, test8, test9, test10, test11];

testCase.forEach(function(item) {
    console.log('Input = ', item);
    console.log('Result = ', parser.parse(item), '\n');
});
